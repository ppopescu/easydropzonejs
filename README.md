# EasyDropzoneJS

EasyDropzoneJS is a Javascript library that makes it easy and fast to add drag-and-drop functionality to your website for file upload functionality or other purposes which require having a file select feature. This way you can easily process file uploads inside a traditional form, without the hassle of writing complex JavaScript or CSS code. While the library is in its early stages, it is stable and provides the basic configurations needed to adapt yo your website. Furthermore, the library is easily themable by providing a simple CSS file with the required theme.

I made this library after needing such functionality and existing solutions (like DropzoneJS) proved too complex or did not have the exact use-case I needed.

You can find a demo on my programming blog: https://petrepopescu.tech

### Features
- Easy to use
- Looks good out of the box
- Easy to customize to suite your website
- Pure JS and CSS, without the need for external libraries like JQuery
- Small size (less than 4kb for both JS and CSS files)
- Preview for images

### Why did I make this? What are the uses cases?

I needed an easy to use drag-and-drop library that does not trigger the upload, but instead is integrated using an existing form, with the image being submitted  together with the form data. I could not find one, so I made my own. 

For example, you are building a registration form where you want to allow the user to upload the profile picture during registration. You can't easily upload the picture prior to registration since you don't know to what user to assign it, and having an extra screen just for the picture is not elegant. So, just include a normal file input and use this library to add the extra functionality for a more modern look-and-feel.

### How to you
Include the CSS and Javascript files:
```html
<link href="default.css" rel="stylesheet" />
<script src="easydropzone.js"></script>
```

Define a div element which contains a file input field. Make sure to include an ID.

```html
<div id="exampleDropzone">
    <input name="file" type="file" accept="image/png, image/jpg, image/jpeg"/>
</div>
```

Instantiate a new EasyDropzone object and provide the container ID and an optional configuration variable:

```html
<script>
    new EasyDropzone('exampleDropzone');
</script>
```

### Configuration
EasyDropzone provides a few optional configuration options that are used mostly for changing the text that is displayed. Below are the default values used:

```javascript
const dropzoneDefaults = {
    clickText: "or click here",
    dragText: "Drag a file here",
    numFilesErrorMessage: "Only one file is allowed",
    numFilesErrorFunction: function(errorMessage) {
        alert(errorMessage);
    },
    invalidFileTypeErrorMessage: "Invalid file type",
    invalidFileTypeErrorFunction: function(errorMessage) {
        alert(errorMessage);
    },
    imagePreview: true,
}
```

If you want to change one or more of the values, insatiate a new configuration variable and provide the desired texts. Pass the configuration variable as the second argument on the constructor. Missing values will be taken from the defaults. Here is an example.

```html
<script>
    var config = {
        clickText: "(or) just click",
        dragText: "Drag an image here",
    }
    new EasyDropzone('exampleDropzoneCustomConfig', config);
</script>
```

### Future enhancements

In the future, the library may be enhanced with extra functionalities and configuration options. Feel free to contribute or suggest features. 
