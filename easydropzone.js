; (function () {
    const dropzoneDefaults = {
        clickText: "or click here",
        dragText: "Drag a file here",
        numFilesErrorMessage: "Only one file is allowed",
        numFilesErrorFunction: function (errorMessage) {
            alert(errorMessage);
        },
        invalidFileTypeErrorMessage: "Invalid file type",
        invalidFileTypeErrorFunction: function (errorMessage) {
            alert(errorMessage);
        },
        imagePreview: true,
    }

    class EasyDropzone {
        config = dropzoneDefaults;
        dropZone;
        fileInput;
        dragSpan;
        browseButton;
        imagePreview;
        constructor(dropzoneIdOrElement, config = {}) {
            this.config = Object.assign({}, dropzoneDefaults, config);
            this.dropZone = dropzoneIdOrElement instanceof HTMLElement ? dropzoneIdOrElement : document.getElementById(dropzoneIdOrElement);
            this.dropZone.classList.add('dropzone');
            const fileInput = this.dropZone.querySelector('input[type=file]');
            if (!fileInput) {
                throw new Error('EasyDropzone requires a file input inside the dropzone element');
            }
            this.fileInput = fileInput
            const children = [];
            this.dragSpan = h('span', { style: 'cursor: default;' }, this.config.dragText);
            children.push(this.dragSpan);
            if (this.config.imagePreview) {
                children.push(this.imagePreview = h('img', { style: 'display: none;' }));
            }

            const browseButton = h('button', { class: 'browseButton' }, this.config.clickText);
            children.push(browseButton);
            this.dropZone.append(...children);

            this.prepareDropzoneEvents();
            browseButton.appendChild(fileInput);
            fileInput.addEventListener('change', (e) => {
                if (fileInput.files.length == 1) {
                    this.dragSpan.textContent = fileInput.files[0].name;
                    this.adjustContainer(fileInput.files);
                } else {
                    this.dragSpan.textContent = this.config.dragText;
                }
            }, false);
        }

        prepareDropzoneEvents() {
            this.dropZone.addEventListener('drop', (e) => {
                preventDefaults(e);
                this.setInput(e);
            }, false);
            this.dropZone.addEventListener('dragover', (e) => {
                preventDefaults(e);
                this.dropZone.classList.add("mouseOver");
            }, false);
            ['dragleave', 'drop'].forEach(eventName => {
                this.dropZone.addEventListener(eventName, (e) => {
                    this.dropZone.classList.remove("mouseOver");
                }, false)
            });
        }

        setInput(event) {
            if (event.dataTransfer.files.length != 1) {
                this.config.numFilesErrorFunction(this.config.numFilesErrorMessage);
                return;
            }
            const acceptedTypes = this.fileInput.accept.split(",").map(type => type.trim());
            const selectedFileType = event.dataTransfer.files[0].type;
            const selectedFiletypeGroup = selectedFileType.split('/')[0];
            if (this.fileInput.accept.length > 0 && !(acceptedTypes.includes(selectedFileType) || acceptedTypes.includes('*/*') || acceptedTypes.includes(`${selectedFiletypeGroup}/*`))) {
                this.config.invalidFileTypeErrorFunction(this.config.invalidFileTypeErrorMessage);
                return;
            }
            this.adjustContainer(event.dataTransfer.files);
        }

        adjustContainer(files) {
            this.fileInput.files = files;
            this.dragSpan.textContent = files[0].name;

            if (this.config.imagePreview) {
                if (isFileImage(files[0])) {
                    this.show(this.imagePreview);
                    this.hide(this.dragSpan);
                    this.imagePreview.src = URL.createObjectURL(files[0]);
                    this.imagePreview.addEventListener('load', () => {
                        URL.revokeObjectURL(this.imagePreview.src);
                    });
                } else {
                    this.show(this.dragSpan);
                    this.hide(this.imagePreview);
                }
            }
        }

        show(element) {
            element.style.display = '';
        }
        hide(element) {
            element.style.display = 'none';
        }

    }

    const isFileImage = (file) => file && file['type'].includes('image/');

    const preventDefaults = (e) => {
        e.preventDefault()
        e.stopPropagation()
    }
    const h = (tag, props, ...children) => {
        const node = document.createElement(tag)
        Object.entries(props).forEach(([key, value]) => {
            if (key.startsWith('on')) {
                node.addEventListener(key.substring(2).toLowerCase(), value)
            } else {
                node.setAttribute(key, value)
            }
        })
        node.append(...children)
        return node
    }
    // Export to global
    window.EasyDropzone = EasyDropzone
})()